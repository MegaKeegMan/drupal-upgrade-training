#!/usr/bin/env bash
set -euo pipefail
[ -d drupal7 ] || mkdir drupal7
cd drupal7
ddev config --docroot=web --project-type=drupal7 --project-name drupal7 --create-docroot
ddev start
rm -rf web
mkdir web
ddev drush dl drupal-7 --destination=/var/www/html --drupal-project-rename=web -y
ddev config --docroot=web --project-type=drupal7 --project-name drupal7
ddev drush si standard -y --account-name=admin --account-pass=admin
mkdir -p web/sites/all/modules/contrib
ddev drush dl addressfield ctools date entity entityreference email telephone token url pathauto views
ddev drush updb
ddev drush cc all
