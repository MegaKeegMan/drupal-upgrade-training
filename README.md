# Setup

This project provides setup assistance for a Drupal 7 to Drupal 9 migration training in a minimum number of steps. It makes use of [DDEV](https://ddev.readthedocs.io/en/stable/). This is the recommended way to participate in the training, but alternative instructions using your preferred environment are at the end of this README.

## DDEV machine setup

### Pre-requisites

  * In order to use the included scripts, a `bash` shell should be available. GNU/Linux and Mac OS will generally work out of the box, for Windows it is recommended to install [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
  * If not already installed, download and install [DDEV](https://ddev.readthedocs.io/en/stable/) and its pre-requisites (for Windows we recommend the WSL2 variant over Docker desktop).
  * Ensure you have `git`; open a command line terminal and type `git --version`.  If you do not get a git version number, [install git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

### Initial project config and ddev containers

All configuration/steps are assumed to be run in a terminal (with the exception of editing the `settings.ddev.php` file). Bash has been the most tested, but zsh/fish/others should function as well.

Here we will clone the project into a new folder (by default `drupal-upgrade-training`) and run two scripts that will configure basic ddev containers with Drupal 7 and 9 for us.

Note that `drupal-upgrade-training` will be referred to as "the project root" in this documentation. Several steps of the documentation use this as a starting/reference point for running commands, meaning you should `cd` to this folder before starting that script section, if the project root is not already your current working directory.

To start, open your terminal, and `cd` to whatever directory you would like to keep this project under, and begin running commands one at a time. If any step reports an error, you should stop and investigate those problems before continuing. This applies throughout this guide, unless a specific note indicates a known error message may be ignored. The install_d9.sh script will cause a confirmation to be displayed (`Warning: ALL EXISTING CONTENT of the project root (~/projects/drupal-upgrade-training/drupal9)`) which you will have to respond to (enter `yes`, or just press enter to overwrite):

```sh
git clone https://gitlab.com/agaric/drupal-upgrade-training.git
cd drupal-upgrade-training
./install_d7.sh
./install_d9.sh
```

Once these steps have completed, verify that you can reach the Drupal7 and Drupal9 sites at these links:

* [Drupal 7 - https://drupal7.ddev.site/](https://drupal7.ddev.site/)
* [Drupal 9 - https://drupal9.ddev.site/](https://drupal9.ddev.site/)

### Notes about additional scripts and ddev ssh

At this point, you should have functional ddev setups for both sites that we will be "going into" and "exiting out of" in the following section of scripts. In essence, this means being inside or back outside of the containers ddev provides. Going into a container occurs whenever you use the command `ddev ssh`. Once you have gone into a ddev container you can then come back out of it with:

* Win/Linux: ctrl+d
* Mac: cmd+d
* The long way everywhere: entering the command `logout`

NOTE: You'll want to start each of these additional sections from your *project root* and *outside* of the ddev container. As a reminder: the default project root is `<your projects-folder>/drupal-upgrade-training`.

### Connect Drupal 9 to Drupal 7 and test

  * Using your preferred editor, add the following to the `drupal9/web/sites/default/settings.ddev.php` file, just after the existing default database configuration (should be around line 29):

```php
$databases['migrate']['default'] = array(
  'database' => "db",
  'username' => "db",
  'password' => "db",
  'host' => "ddev-drupal7-db",
  'driver' => "mysql",
  'port' => $port,
  'prefix' => "",
);
```

Now, ensure you can reach the Drupal7 database from the Drupal9 ddev container:

```sh
cd drupal9
ddev ssh
drush sqlc --database=migrate
```

You should get a `MariaDB [db]>` prompt indicating you are in the MariaDB client, connected to the Drupal 7 database. Press ctrl+d/cmd+d, or type `quit` to exit the MariaDB client. After exiting MariaDB, you will still be in the ddev container - stay in the container for the very next command (curl).

Test reaching the D7 web site from within Drupal9 container by running: `curl -o /tmp/logo.png http://ddev-drupal7-web/themes/bartik/logo.png`

This should download the Drupal7 logo to a temp folder without error. Exit the ddev container.

### Load the source D7 database and resources

Download the D7 database SQL file and load it into the ddev container:
```
cd drupal7
curl -o drupal7.sql.gz https://raw.githubusercontent.com/dinarcon/ud-drupal-upgrade/main/drupal7/drupal7.sql.gz
ddev import-db --src=drupal7.sql.gz
ddev drush updb -y
cd ..
```

Get the D7 files (images) and put them in place (`sites/default/files`):

```
curl -o drupal7/web/sites/default/files.tgz https://raw.githubusercontent.com/dinarcon/ud-drupal-upgrade/main/drupal7/files.gz
tar xf drupal7/web/sites/default/files.tgz --directory=drupal7/web/sites/default/
```
Visit your [Drupal 7](https://drupal7.ddev.site/) site and you should have content with images.

### Add the custom migration module

Set up the Drupal 9 site with a migration module and enable it and modules it requires:

```
cd drupal9
ddev ssh
composer config minimum-stability dev
composer config repositories.dinarcon/ud-drupal-upgrade git https://github.com/dinarcon/ud-drupal-upgrade.git
composer require "drupal/ud_drupal_upgrade"
drush en ud_drupal_upgrade -y
```

You can now test that migrations are functional (from the drupal9 folder). Note that running these command may result in an error that may be ignored which looks like this:

```
 [error]  Message: Failed to connect to your database server. The server reports the following 
message: /No database connection configured for source plugin variable/.
 * Is the database server running?
 * Does the database exist, and have you entered the correct database name?
 * Have you entered the correct username and password?
 * Have you entered the correct database hostname?
 ```

* `ddev drush mim --group migrate_drupal_7` should run the migrations
* `ddev drush ms` will display the migration status
* `ddev drush mr --group migrate_drupal_7` will rollback the migration.

## DIY requirements

A Drupal 7 site, with the following modules available. This database should be re-loaded from our sample source, and the files should also be downloaded into the public location:

```
addressfield ctools date entity entityreference email telephone token url pathauto views
```

A Drupal 8/9 site, managed by composer, with the following modules:

```
composer require "drupal/devel:^4.1"
composer require "drupal/address:^1.9"
composer require "drupal/migrate_plus:^5.1"
composer require "drupal/migrate_tools:^5.0"
composer require "drupal/migrate_upgrade:^3.2"
composer require "drupal/paragraphs:^1.12"
composer require "drupal/social_link_field:^1.1@alpha"
composer require "drush/drush:~10.3.0"
```

You might need to change the `minimum-stability` to install the `drupal/social_link_field` module.

```
composer config minimum-stability dev`
```

# Repositories:

  * https://gitlab.com/agaric/drupal-upgrade-training
  * https://github.com/dinarcon/ud-drupal-upgrade


# Helpful commands

Generate the migrations:

```
drush en migrate_upgrade
drush --yes migrate:upgrade --legacy-db-key='migrate' --legacy-root=http://ddev-drupal7-web/ --configure-only
drush config:export
```

Import the configuration for changes to the import module (from the drupal9 folder):

```
ddev drush config-import --partial --source=modules/contrib/ud_drupal_upgrade/config/install
```

# Shared notes

Put your own tips and errors or other problems you run into here!

https://pad.drutopia.org/p/agaric-drupal-upgrade-training


# Troubleshooting

Solutions to known issues are documented in [TROUBLESHOOTING.md](TROUBLESHOOTING.md)

