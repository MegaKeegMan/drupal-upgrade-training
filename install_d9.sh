#!/usr/bin/env bash
set -euo pipefail
[ -d drupal9 ] || mkdir drupal9
cd drupal9
ddev config --docroot=web --project-type=drupal9 --project-name drupal9 --create-docroot
ddev start
ddev composer create -n "drupal/recommended-project"
ddev composer require -n "drush/drush:10.3.6"
ddev drush site:install -y --account-name=admin --account-pass=admin
ddev composer require -n "drupal/devel:^4.1.1" "drupal/address:^1.9" "drupal/migrate_plus:^5.1" "drupal/migrate_tools:^5.0" "drupal/migrate_upgrade:^3.2" "drupal/paragraphs:^1.12"